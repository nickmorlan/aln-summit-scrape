summit_scrape.py
===
The scraper for www.summitohioprobate.com is a python script that utilizes the Splinter library
and PhantomJS for automating and traversing web pages that rely on javascript for navigation. 

A python 2.7.8 bundle in installed at /user/local/bin/python2.7 and it used instead of the default
sytem python 2.6 distro.  There are some Rackspace tools that depend on 2.6 so it was left alone.
Splinter requres 2.7 or greater.

To run from the command line use 'python2.7 scrape_summit.py all 20141111 20141111'. The script 
takes three parameters, the first being the case type, and the last two being the date range in
YYYYMMDD format.

#####Supported Types
>    "namechange" 
>    "civil"         
>    "conservatorship"  
>    "estate"
>    "guardianshipadult"
>    "guardianshipminor"
>    "releaseadmin"
>    "trust"
>    "willrecord"
>    "all"


It can be called from the command line