#!/usr/local/bin/python2.7
# system libs used
import traceback
import argparse
import sys
from os import chmod
from collections import OrderedDict
from datetime import date, timedelta

# make sure these libraires are present
try:
    # headless browser
    # note that splinter needs python 2.7+ and the centos install for black
    # requires python 2.6.6 for certain tasks(namely yum) so a second 2.7.6 interpreter
    # is installed and used for this script
    # Also installed npm(node) to install phantomjs
    from splinter import Browser
except ImportError:
    raise ImportError('Missing Browser library. See http://splinter.cobrateam.info/docs/install.html')
    sys.exit()
try:
    # document tree parser
    from bs4 import BeautifulSoup
except ImportError:
    raise ImportError('Missing BueatifulSoup data parsing library. See http://www.crummy.com/software/BeautifulSoup/#Download')
    sys.exit()


# set up param parser
parser = argparse.ArgumentParser(description='''Scrape www.summitohioprobate.com estate notices based on range of filing dates.
     Use command like test.py estate 20140101 20140101\n Use test.py all 00000000 00000000 to autoscrape all cases from yesterday.''')
parser.add_argument('type', help='Case type to search')
parser.add_argument('start', help='Starting file date in YYYYMMDD format')
parser.add_argument('end', help='Ending file date in YYYYMMDD format')
args = parser.parse_args()

# parameter data validation.. just in case it is ever manually run from the command line
valid_search_types = {
    "namechange":        "CN        ",
    "civil":             "CV        ",
    #"civilinre":         "CVRE      ",
    "conservatorship":   "CO        ",
    #"births":            "BR        ",
    "estate":            "ES        ",
    "guardianshipadult": "GA        ",
    "guardianshipminor": "GM        ",
    #"marraige":          "ML        ",
    #"miscellaneous":     "MS        ",
    "releaseadmin":      "ER        ",
    "trust":             "TR        ",
    #"willdeposit":       "WD        ",
    "willrecord":        "WR        ",
    "all":               "ALL"
}
if args.type.lower() not in valid_search_types:
    exit(':Invalid case type -{}'.format(args.type))
if not args.start.isdigit() or not len(args.start) == 8:
    exit(':Start must be in the format of YYYYMMDD -{}'.format(args.start))
if not args.end.isdigit() or not len(args.end) == 8:
    exit(':Start must be in the format of YYYYMMDD -{}'.format(args.end))
# check for zero date and set to run for yesterday
if args.start == "00000000":
    yesterday  = date.today() - timedelta(days = 1)
    args.start = yesterday.strftime('%Y%m%d')
    args.end   = args.start
# validation passed.. set the search parameters
search_type  = valid_search_types[args.type.lower()]
search_start = args.start[4:6] + '/' + args.start[6:8] + '/' + args.start[0:4]
search_end   = args.end[4:6] + '/' + args.end[6:8] + '/' + args.end[0:4]
#for saving files..
root_path    = '/var/www/vhosts/blackcreeksolutions.com/alnscrapers.blackcreeksolutions.com/'
file_name    = root_path + 'scrapesummit/summitprobate' + args.start + '.txt'
errlog       = root_path + 'errlog.txt'
#exit()

# An empy holder class to easily assign properties and then
# get a json representation of the object if needed
# Also contains a few static helper methods to format data
class CourtRecord():
    def __init__(self):
        self.case_number        = ''
        self.case_number_year   = ''
        self.case_number_index  = ''
        self.case_number_type   = ''
        self.case_number_suffix = ''
        self.case_type          = ''
        self.file_date          = ''
        self.judge              = ''
        self.action              = ''
        self.decedent           = ''
        self.party1             = ''
        self.party2             = ''
        self.party3             = ''
        self.party1_type        = ''
        self.party2_type        = ''
        self.party3_type        = ''
        self.attorney           = ''
        self.docket_text1       = ''
        self.docket_text2       = ''

    # fixes last name comma first and looks for a few exceptions
    # on common business names
    @staticmethod
    def fix_last_name_first(courtname):
        et_al = ''
        name = courtname
        formatted = ''
        if courtname[-6:].lower() == ' et al':
            name = courtname[:-6]
            et_al = ' et al'

        parts = [x.strip() for x in name.split(',')]
        num = len(parts)

        if num == 1:
            return name + et_al
        elif num == 2:
            if parts[1].upper() in ['LLC', 'LLC.', 'INC', 'INC.']:
                return name + et_al
            else:
                return parts[1] + ' ' + parts[0] + et_al
        elif num == 3:
            for i in range(0, num - 1):
                formatted = formatted + ' ' + parts[i]
            return parts[num - 1] + formatted
        else:
            return name + et_al

    # returns a plural form of the given word following generic rules..
    # won't handle oddball pluralizations .. could add special cases if needed
    @staticmethod
    def pluralize(word):
        if word[-1:] != 'y':
            return word + 's'
        else:
            return word[:-1] + 'ies'

# end CourtRecord class definition

# load the browser to render pages with javascript
#browser = Browser('firefox')
browser = Browser('phantomjs') # headless
url     = 'http://www.summitohioprobate.com/eservices/'
# list to hold scraped info
records = [] 

try:
    # start scrape
    browser.visit(url)
    if browser.is_element_present_by_name('linkFrag:beginButton', 5):
        # the initial terms acceptance page has rendered
        # now click the button
        browser.click_link_by_partial_text('Click Here')
        if browser.is_text_present('Case Type', 5):
            if browser.is_element_present_by_name('caseCd', 5):
                # the initial search page has rendered
                # now to click the tab to search by case type
                browser.click_link_by_partial_text('Case Type')
                # give the form some values and submit
                browser.fill_form({"fileDateRange:beginDate" : search_start, "fileDateRange:endDate": search_end})
                options = browser.find_by_name('caseCd')
                if search_type == "ALL":
                    for k, v in valid_search_types.items():
                        if k != 'all':
                            options.select(v)
                else:
                    options.select(search_type)
                browser.find_by_name("submitLink").click()

                if browser.is_text_present('Search Results', 5):
                    # the search results page has rendered
                    # grab all the links elements within the td.bookmarkablePageLinkPropertyColumnLink
                    # these link to the case details
                    links = []
                    has_more_results = True
                    # looking for multiple pages of resutlts..
                    while has_more_results:
                        has_more_results = False
                        case_links = browser.find_by_css('td.bookmarkablePageLinkPropertyColumnLink a')
                        for b in browser.find_by_css('td.bookmarkablePageLinkPropertyColumnLink a'):
                            if b['href'] not in links:
                                links.append(b['href'])
                        
                        # click the next results page link if there is one there and make our condition true
                        if browser.is_element_present_by_css('.navigator', 5):
                            if len(browser.find_link_by_text('>')) > 0:
                                has_more_results = True
                                browser.click_link_by_text('>')
                                dummy_pause = browser.is_text_present('Search Results', 5)
                    # end while has_more_results:

                    # loop through the links
                    for link in links:    
                        browser.visit(link)
                        if browser.is_element_present_by_css('.caseInfoItems', 10):
                            # we have arrived on the page to scrape
                            # set up a holder object
                            record = CourtRecord()
                            # BeautifulSoup parses and traverses the document tree
                            soup = BeautifulSoup(browser.html)

                            # see test.py.format.text for the document structure of the 
                            # caseHeaderDiv, partiesDiv and docketDiv objects
                            caseHeaderDiv = soup.find('div', id='caseDetailHeader')

                            case_number = caseHeaderDiv.div.h2.contents[0]
                            case_number = case_number.strip()
                            parts = case_number.split()
                            record.case_number        = case_number
                            record.case_number_year   = parts[0]
                            record.case_number_type   = parts[1]
                            record.case_number_index  = parts[2]
                            if len(parts) == 4:
                                record.case_number_suffix = parts[3]

                            # grab case info from header within dt and dd elements
                            ## uses next-sibling twice sine the newlines are an element
                            for el in caseHeaderDiv.findAll(['dt']):
                                if len(el.contents) > 0:
                                    if el.contents[0].strip() == 'Case Type':
                                        record.case_type = el.next_sibling.next_sibling.contents[0].strip()
                                    elif el.contents[0].strip() == 'File Date:':
                                        record.file_date = el.next_sibling.next_sibling.contents[0].strip()
                                    elif el.contents[0].strip() == 'Action:':
                                        record.action = el.next_sibling.next_sibling.contents[0].strip()
                                    elif el.contents[0].strip() == 'Case Judge:':
                                        judge = el.next_sibling.next_sibling.contents[0].strip()
                                        record.judge = CourtRecord.fix_last_name_first(judge)


                            partiesDiv = soup.find('div', id='ptyContainer')
                            # loop through each listed party row and grab the info
                            parties = OrderedDict()
                            # hack to force order of parties (dumb since they are labeled)
                            if (record.case_number_type == 'ER' or record.case_number_type == 'ES'
                            	or record.case_number_type == 'TR' or record.case_number_type == 'WR'
                            	or record.case_number_type == 'ES'):
                            	parties['decedent'] = []
                            elif (record.case_number_type == 'GA' or record.case_number_type == 'GM'):
                            	parties['ward'] = []
                            elif (record.case_number_type == 'CN'):
                            	parties['old name'] = []
                            elif (record.case_number_type == 'CO'):
                            	parties['conservatee'] = []
                            elif (record.case_number_type == 'CV'):
                            	parties['plaintiff'] = []


                            attorneys = []
                            for d in partiesDiv.findAll('div', {'class':['rowodd', 'roweven']}):
                                # attorney check
                                if hasattr(d.dl, 'dt'):
                                    if d.dl.dt.contents[0].strip() == 'Attorney':
                                        attorney = d.dl.dd.contents[0].strip()
                                        atty = CourtRecord.fix_last_name_first(attorney)
                                        if atty not in attorneys:
                                            attorneys.append(atty)

                                # party and party type check
                                if d.h5 is None:
                                    continue

                                partyType = d.h5.span.contents[0]
                                partyType = partyType.replace('-', '').strip().lower()
                                party     = d.h5.contents[0]
                                party     = CourtRecord.fix_last_name_first(party.strip())

                                if partyType in parties.items():
                                    if party not in parties[partyType]:
                                        parties[partyType].append(party)
                                else:
                                    parties.setdefault(partyType, []).append(party)
                            # end for d in partiesDiv..

                            record.attorney = "; ".join(attorneys)
                            # set parties and party type in the proper fields
                            i = 1
                            for k, v in parties.items():
                                # pluralize the type if there more than one party for it
                                if len(v) > 1:
                                    k = CourtRecord.pluralize(k)
                                setattr(record, "party" + str(i), "; ".join(v))
                                setattr(record, "party" + str(i) + "_type", k.capitalize())
                                i = i + 1

                            # grab the first and second docket entry rows
                            docketDiv = soup.find('div', id='docketInfo')
                            counter = 1
                            for row in docketDiv.findAll('td', {'scope': 'row'}):
                                if counter == 1:
                                    record.docket_text1 = row.contents[0] 
                                    record.docket_text1 = record.docket_text1 + ' ' + row.parent.find('td', {'class': 'formattedText'}).contents[0].replace('\n', ' ')
                                    amount = row.parent.find('td', {'class': 'currency'})
                                    if len(amount.contents) > 0:
                                        record.docket_text1 = record.docket_text1 + ' ' + amount.contents[0]
                                    record.docket_text1 = record.docket_text1.strip()
                                elif counter == 2:
                                    record.docket_text2 = row.contents[0] 
                                    record.docket_text2 = record.docket_text2 + ' ' + row.parent.find('td', {'class': 'formattedText'}).contents[0].replace('\n', ' ')
                                    amount = row.parent.find('td', {'class': 'currency'})
                                    if len(amount.contents) > 0:
                                        record.docket_text2 = record.docket_text2 + ' ' + amount.contents[0]
                                    record.docket_text2 = record.docket_text2.strip()
                                elif counter == 3:
                                    break
                                counter = counter + 1
                            # end for row in docketDiv

                            #print vars(record) # debug final scrape values
                            # add scraped object to the object list
                            records.append(record)
                        # end if browser.is_element_present_by_css('.caseInfoItems', 10):
                    # end for link in links:  
                    
                    # write a results file..
                    f = open(file_name, 'w')
                    f.write('case_number\tcase_number_year\tcase_number_type\tcase_number_index\tcase_number_suffix\tcase_type\tfile_date\tjudge\taction\tparty1\tparty1_type\tparty2\tparty2_type\tparty3\tparty3_type\tattorney\tdocket_text1\tdocket_text2\n')
                    for record in records:
                        f.write(record.case_number + '\t')
                        f.write(record.case_number_year + '\t')
                        f.write(record.case_number_type + '\t')
                        f.write(record.case_number_index + '\t')
                        f.write(record.case_number_suffix + '\t')
                        f.write(record.case_type + '\t')
                        f.write(record.file_date + '\t')
                        f.write(record.judge + '\t')
                        f.write(record.action + '\t')
                        f.write(record.party1 + '\t')
                        f.write(record.party1_type + '\t')
                        f.write(record.party2 + '\t')
                        f.write(record.party2_type + '\t')
                        f.write(record.party3 + '\t')
                        f.write(record.party3_type + '\t')
                        f.write(record.attorney + '\t')
                        f.write(record.docket_text1 + '\t')
                        f.write(record.docket_text2 + '\n')
                    f.close()
                    chmod(file_name, 0774)
               # end if search results present

        else: # if browser.is_text_present('Case Type', 5):
            f = open('errlog.txt', 'a')
            f.write(date.today().strftime('%Y-%m-%d') + '\nInitial page structure may have changed.\n\n')
            f.close()


        # end browser session
        browser.quit()
  
    else: # if browser.is_element_present_by_name('linkFrag:beginButton', 5):
        f = open(errlog, 'a')
        f.write(date.today().strftime('%Y-%m-%d') + '\nCould not load initail search page @ ' + url + '\n\n')
        f.close()

except AttributeError as e:
    browser.quit()
    txt = "An Error occurred, expected different document structure - \n"
    txt = txt + traceback.format_exc()
    f = open(errlog, 'a')
    f.write(date.today().strftime('%Y-%m-%d') + '\n' + txt + '\n\n')
    f.close()
    exit()

except StandardError as e:
    browser.quit()
    txt = "An Error occurred - \n"
    txt = txt + traceback.format_exc()
    f = open(errlog, 'a')
    f.write(date.today().strftime('%Y-%m-%d') + '\n' + txt + '\n\n')
    f.close()
    exit()
